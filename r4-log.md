# #100DaysOf[No]Code Log - Round 4 by CodeFreezr

<img src="assets/100DaysOfCodeR4-Logo.jpg"/>

The log of my #100DaysOf[No]Code, Round 4, challenge.

---
## Preperation

Collecting the Space in a Mindmap
Starting an awesome NoCode-and-LowCode NoLow

### Curiculum

1. NoCode  & lowcode > awesome #NoLow
1. GameDev & 3DCG  for multimedia storytelling (unreal, unity, godot & blender)
1. Jupyter polyglot Notebooks
1. groovy again perhaps onGraals, Inside Apps, Serverless


### Todos
Collect Minions, Minions and more Minions (SVG, 3D, PNG)


### First Experiments done with

GlideApp

Blueprint

Zapier ~13.6

- Github > Gmail
- Twitter > Google Sheet
- Github <> Gitlab (Idea)
- Github.Gist <> Gitlab.Snippet

### Preparing Tweets:

* [Day-0 minus 10](https://twitter.com/DetlefBurkhardt/status/1271887352848429057?s=20)
* [Day-0 minus 9](https://twitter.com/DetlefBurkhardt/status/1272265473242038273?s=20)
* [Day-0 minus 8a](https://twitter.com/DetlefBurkhardt/status/1272621507764785158?s=20)
* [Day-0 minus 8b](https://twitter.com/DetlefBurkhardt/status/1281944838708047881?s=20)
* [Day-0 minus 7](https://twitter.com/DetlefBurkhardt/status/1282442098898337799?s=20)
* [Day-0 minus 6](https://twitter.com/DetlefBurkhardt/status/1282821367575052301?s=20)
* [Day-0 minus 5](https://twitter.com/DetlefBurkhardt/status/1283172953522307073?s=20)
* [Day-0 minus 4](https://twitter.com/DetlefBurkhardt/status/1283524557492625409?s=20)
* [Day-0 minus 2](https://twitter.com/DetlefBurkhardt/status/1284273578247102464?s=20)
* [Day-0 minus 1](https://twitter.com/DetlefBurkhardt/status/1284568176878026753?s=20)


### Twitter Moment for R4 ✨:
* [Twitter Moment for my 100 Days Of NoCode R4 (Part One)](http://bit.ly/cf-r4-mmnt01)
* [Twitter Moment for my 100 Days Of NoCode R4 (Part Two)](http://bit.ly/cf-r4-mmnt02)

### Playlists:
* [YT List about Blender VSE](https://bit.ly/b3d-vse)
* [YT List about Blender Audio](https://bit.ly/b3d-audio)
* [YT List about Blender Physics](https://bit.ly/b3d-physics)
* [YT List about Blender Procedural & Generative Arts](https://bit.ly/b3d-proc) - 33 - [R4:63](https://twitter.com/DetlefBurkhardt/status/1308193198234374145?s=20)
* [YT List about Blender Fundamental](https:/bit.ly/fun-28)
* [YT List about Blender HDRI](https://bit.ly/b3d-HDRI) - 25 - [R4:66.1](https://twitter.com/DetlefBurkhardt/status/1309896911122563074?s=20)
* [YT List about Blender Addons](https://bit.ly/b3d-addons) - 23 - [R4:72.1](https://twitter.com/DetlefBurkhardt/status/1312339204744019974?s=20)

tbd:
* [YT List about Blender Basics - 2]()
* [YT List about Blender Paradox - 3]()
* [YT List about Blender MoCap - 1]()
* [YT List about Blender Awesome - 15]()
* [YT List about Blender Python - 7]()
* [YT List about Blender 4K - 0]()



### Blog & Repo:
* [R4 Starter Blog-Post](http://bit.ly/cf-r4)
* [My dirty little Blenderlab Repo](https://bit.ly/cf-b-lab)


### Daily Tweets for R4: (Part One)
* [Day 0: Introduction](https://twitter.com/DetlefBurkhardt/status/1284956936878923777?s=20)
* [Day 1: Learning Unreal](https://twitter.com/DetlefBurkhardt/status/1285300115553558528?s=20)
* [Day 2: My first 5 Badges](https://twitter.com/DetlefBurkhardt/status/1285696157302300674?s=20)
* [Day 3: 75% Getting Started](https://twitter.com/DetlefBurkhardt/status/1286058315345211398?s=20)
* [Day 4: Getting Started 100% finished](https://twitter.com/DetlefBurkhardt/status/1286409149337743368?s=20)
* [Day 5: Starting Blueprint Learning Path](https://twitter.com/DetlefBurkhardt/status/1286765598257676289?s=20)
* [Day 6: Analysing Blueprint Basic Structures](https://twitter.com/DetlefBurkhardt/status/1287081093787770880?s=20)
* [Day 7: Got 50% on Exploring Blueprint](https://twitter.com/DetlefBurkhardt/status/1287400916505829376?s=20)
* [Day 8: Missing Doxygen for Overview](https://twitter.com/DetlefBurkhardt/status/1287900421420462080?s=20)
* [Day 9: Blueprint Paperback arrived](https://twitter.com/DetlefBurkhardt/status/1288169130722578432?s=20)
* [Day 10: git clone unrealengine](https://twitter.com/DetlefBurkhardt/status/1288583265025228806?s=20)
* [Day 11: 75% exploring bp](https://twitter.com/DetlefBurkhardt/status/1288893231569293314?s=20)
* [Day 12: collapse bp](https://twitter.com/DetlefBurkhardt/status/1289309665440874498?s=20)
* [Day 13: bolt](https://twitter.com/DetlefBurkhardt/status/1289688592910974978?s=20)
* [Day 14: unreal in wasm](https://twitter.com/DetlefBurkhardt/status/1290038842305609728?s=20)
* [Day 15: starts bolt game](https://twitter.com/DetlefBurkhardt/status/1290434139582341128?s=20)
* [Day 16: between-the-engines](https://twitter.com/DetlefBurkhardt/status/1290807572082167809?s=20)
* [Day 17: no nocode today: too hot](https://twitter.com/DetlefBurkhardt/status/1291123484240814081?s=20)
* [Day 18: Finishing RoboCubeBall today](https://twitter.com/DetlefBurkhardt/status/1291515114693296129?s=20)
* [Day 19: just type: scoop install godot](https://twitter.com/DetlefBurkhardt/status/1291892843049615360?s=20)
* [Day 20: Tried Godot Circle Pop as webassembly](https://twitter.com/DetlefBurkhardt/status/1292144284712476688?s=20)
* [Day 21: My very first day on itch.io. Awesome!](https://twitter.com/DetlefBurkhardt/status/1292524445601615872?s=20)
* [Day 22: Some Chapters Exploring Blueprint](https://twitter.com/DetlefBurkhardt/status/1292941447877066755?s=20)
* [Day 23: More Chapters on Blueprint](https://twitter.com/DetlefBurkhardt/status/1293319828690927616?s=20)
* [Day 24: Tabledata in Blueprint](https://twitter.com/DetlefBurkhardt/status/1293663659844132864?s=20)
* [Day 25: More in Blueprint](https://twitter.com/DetlefBurkhardt/status/1293993470634467329?s=20)
* [Day 26: Exploring 100%](https://twitter.com/DetlefBurkhardt/status/1294586084463202305?s=20)
* [Day 27: music visual](https://twitter.com/DetlefBurkhardt/status/1294764455033806851?s=20)
* [Day 28: more bolt](https://twitter.com/DetlefBurkhardt/status/1295029003913433088?s=20)
* [Day 29: engines](https://twitter.com/DetlefBurkhardt/status/1295454218615246848?s=20)
* [Day 30: unity2godot](https://twitter.com/DetlefBurkhardt/status/1295855434688081920?s=20)
* [Day 31: learning2learn](https://twitter.com/DetlefBurkhardt/status/1296244165819367431?s=20)
* [Day 32: goodsky 4 unreal](https://twitter.com/DetlefBurkhardt/status/1296557099577675778?s=20)
* [Day 33: cinemaschine with bolt](https://twitter.com/DetlefBurkhardt/status/1296947193140715521?s=20)
* [Day 33: tracking shot cinemaschine](https://twitter.com/DetlefBurkhardt/status/1297320604794851331?s=20)
* [Day 34: the final tracking shot with bolt](https://twitter.com/DetlefBurkhardt/status/1297320604794851331?s=20)
* [Day 35: the first hours in blender3D: awesome!](https://twitter.com/DetlefBurkhardt/status/1297681240200445964?s=20)
* [Day 36: 600 Monkey in less the 12 lines of code](https://twitter.com/DetlefBurkhardt/status/1298031807053668353?s=20)
* [Day 37: from boring brick to hunderwasser](https://twitter.com/DetlefBurkhardt/status/1298412792400285696?s=20)
* [Day 38: exploring lights and materials](https://twitter.com/DetlefBurkhardt/status/1298756555085623298?s=20)
* [Day 39: neonlight.. shimmering neoner](https://twitter.com/DetlefBurkhardt/status/1299114944521293824?s=20)
* [Day 40: more python in blender3D](https://twitter.com/DetlefBurkhardt/status/1299502431034847233?s=20)
* [Day 41: my first blender addon](https://twitter.com/DetlefBurkhardt/status/1299839640107724810?s=20)
* [Day 42: back to the basic](https://twitter.com/DetlefBurkhardt/status/1300209556153344000?s=20)
* [Day 43: blender 2.9](https://twitter.com/DetlefBurkhardt/status/1300566799948410880?s=20)
* [Day 44: bpy course](https://twitter.com/DetlefBurkhardt/status/1300932888616153089?s=20)
* [Day 45: polyfjord](https://twitter.com/DetlefBurkhardt/status/1301317158606176259?s=20)
* [Day 46: 29#47](https://twitter.com/DetlefBurkhardt/status/1301976430931251201?s=20)
* [Day 47.1: rigging](https://twitter.com/DetlefBurkhardt/status/1302190760104689665?s=20)
* [Day 47.2: goldragon](https://twitter.com/DetlefBurkhardt/status/1302263369181822976?s=20)
* [Day 47.3: weight paint](https://twitter.com/DetlefBurkhardt/status/1302327747931500545?s=20)
* [Day 47.4: 37#47 IK ready](https://twitter.com/DetlefBurkhardt/status/1302392679985278976?s=20)
* [Day 48.1: Keyframes, Timeline](https://twitter.com/DetlefBurkhardt/status/1302555700669939714?s=20)
* [Day 48.2: Blender PowerSequencer](https://twitter.com/DetlefBurkhardt/status/1302689650193182721?s=20)
* [Day 48.3: B3D Python in Sequencer](https://twitter.com/DetlefBurkhardt/status/1302714608193593347?s=20)
* [Day 49: Running rigged wolf in wireframe](https://twitter.com/DetlefBurkhardt/status/1303116415453585411?s=20)
* [Day 50: My first bad and ugly sculpted marble](https://twitter.com/DetlefBurkhardt/status/1303475349993525249?s=20)
* [Day 51.1: Import and plotting CSV multidimensional](https://twitter.com/DetlefBurkhardt/status/1303781247177678849?s=20)
* [Day 51.2: Finishing Blender Fundamentals 2.8](https://twitter.com/DetlefBurkhardt/status/1303823269548756993?s=20)
* [Day 52: My first minutes with BY-GEN Addon](https://twitter.com/DetlefBurkhardt/status/1304202549650698244?s=20)
* [Day 53: Blender3D Animation with Python](https://twitter.com/DetlefBurkhardt/status/1304505003978166272?s=20)
* [Day 54.1: Turnaround Camera is there](https://twitter.com/DetlefBurkhardt/status/1304788407680466945?s=20)
* [Day 54.2: Starting Blender3D Audio](https://twitter.com/DetlefBurkhardt/status/1304877963591254017?s=20)
* [Day 54.2: Bake Sound to F-Curves](https://twitter.com/DetlefBurkhardt/status/1304913675229290501?s=20)
* [Day 55: reactive Sound in b3d](https://twitter.com/DetlefBurkhardt/status/1305146688189890560?s=20)

### Daily Tweets for R4: (Part Two)
* [Day 56: Pyramid as primitive](https://twitter.com/DetlefBurkhardt/status/1305542033331728384?s=20)
* [Day 57: 1000 Pyramids in 10 loc](https://twitter.com/DetlefBurkhardt/status/1306011340738306048?s=20)
* [Day 58: First Physics with domino](https://twitter.com/DetlefBurkhardt/status/1306360419829907459?s=20)
* [Day 59: My First Startup File with Mats](https://twitter.com/DetlefBurkhardt/status/1306705410389401602?s=20)
* [Day 60: Bmesh is mighty and realy an easy](https://twitter.com/DetlefBurkhardt/status/1307091226995421185?s=20)
* [Day 61.1: converting absolute path to relative](https://twitter.com/DetlefBurkhardt/status/1307302553324056579?s=20)
* [Day 61.2: founding my blenderlab: a dirty raw repo](https://twitter.com/DetlefBurkhardt/status/1307379225045893125?s=20)
* [Day 61.3: Another Brick In The Wall, a simulation](https://twitter.com/DetlefBurkhardt/status/1307434297754816512?s=20)
* [Day 62.1: Playlist for Physics in Blender](https://twitter.com/DetlefBurkhardt/status/1307618529378787329?s=20)
* Day 62.2: n/a
* [Day 62.3: Python Migration Cheat](https://twitter.com/DetlefBurkhardt/status/1307677969117310976?s=20)
* [Day 62.4: Sorcar rocks](https://twitter.com/DetlefBurkhardt/status/1307822704230379520?s=20)
* [Day 63: b3d-proc](https://twitter.com/DetlefBurkhardt/status/1308193198234374145?s=20)
* [Day 64: steam](https://twitter.com/DetlefBurkhardt/status/1308913710237462528?s=20)
* [Day 65: teapot](https://twitter.com/DetlefBurkhardt/status/1309611188385386503?s=20)
* [Day 66.1: HDRI](https://twitter.com/DetlefBurkhardt/status/1309896911122563074?s=20)
* [Day 66.2: EZ HDRI](https://twitter.com/DetlefBurkhardt/status/1309939205062230016?s=20)
* [Day 66.3: teapot HDRI](https://twitter.com/DetlefBurkhardt/status/1309991884253794305?s=20)
* [Day 67.1: paradox HDRI](https://twitter.com/DetlefBurkhardt/status/1310146445132079105?s=20)    
* [Day 67.2: paradox Python](https://twitter.com/DetlefBurkhardt/status/1310284679321444357?s=20)
* [Day 67.2: paradox Animation](https://twitter.com/DetlefBurkhardt/status/1310371573258489856?s=20)
* [Day 68: Blender Shortcuts 2.9](https://twitter.com/DetlefBurkhardt/status/1310699185314902026?s=20)
* [Day 69: Blender Render on Docker](https://twitter.com/DetlefBurkhardt/status/1311066430281003013?s=20)
* [Day 70: Blender Commandline Interface](https://twitter.com/DetlefBurkhardt/status/1311418186793922561?s=20)
* [Day 71: Measuring Blender bpy module](https://twitter.com/DetlefBurkhardt/status/1312235124788998145?s=20)
* [Day 72.1: YT-PL on Blender Addons](https://twitter.com/DetlefBurkhardt/status/1312339204744019974?s=20)
* [Day 72.2: Stone Generator Addon](https://twitter.com/DetlefBurkhardt/status/1312386621308833792?s=20)
* [Day 72.3: Stone-Experiments](https://twitter.com/DetlefBurkhardt/status/1312466247032868865?s=20)
* [Day 73.1: Back 2 Engine](https://twitter.com/DetlefBurkhardt/status/1312713776957972480?s=20)
* [Day 73.2: ue4 roadmap](https://twitter.com/DetlefBurkhardt/status/1312745026242437121?s=20)
* [Day 73.3: first AR](https://twitter.com/DetlefBurkhardt/status/1312782120931258369?s=20)
* [Day 73.4: City Park](https://twitter.com/DetlefBurkhardt/status/1312834493464674305?s=20)
* [Day 73.5: UE 4.26 pre](https://twitter.com/DetlefBurkhardt/status/1312893346717798401?s=20)
* [Day 74: UE4 B3D Livelink](https://twitter.com/DetlefBurkhardt/status/1313246183284969474?s=20)
* [Day 75: Epic FFTM October](https://twitter.com/DetlefBurkhardt/status/1313559783157501957?s=20)
* [Day 76: Unreal Python Scripting](https://twitter.com/DetlefBurkhardt/status/1314349885639077889?s=20)
* [Day 77: Unreal Pythin Youtube Playlist](https://twitter.com/DetlefBurkhardt/status/1314689304925532160?s=20)
* [Day 78.1: Sudden End of my short Liason](https://twitter.com/DetlefBurkhardt/status/1314823779739152385?s=20)
* [Day 78.2: Unreal Livelink to Blender works](https://twitter.com/DetlefBurkhardt/status/1315016765135609861?s=20)
* [Day 79: eat-stack as a stackshare stack](https://twitter.com/DetlefBurkhardt/status/1315398139818528769?s=20)
* [Day 80: twitter list for asset stores](https://twitter.com/DetlefBurkhardt/status/1315763445590118400?s=20)
* [Day 81: C4 & UML for gamedevelopment](https://twitter.com/DetlefBurkhardt/status/1316141480730296320?s=20)
* [Day 82: Cloudbased games with K8s](https://twitter.com/DetlefBurkhardt/status/1316776765780615171?s=20)
* [Day 83: Blender Python Playlist](https://twitter.com/DetlefBurkhardt/status/1317599828231311360?s=20)
* [Day 84: Blender3D as portable](https://twitter.com/DetlefBurkhardt/status/1318965500882407425?s=20)
* [Day 85: More Documentation](https://twitter.com/DetlefBurkhardt/status/1319731803696078849?s=20)
* [Day 86: Modeling UML](https://twitter.com/DetlefBurkhardt/status/1320115709796601856?s=20)
* [Day 87.1: VP1](https://twitter.com/DetlefBurkhardt/status/1320264632402124805?s=20)
* [Day 87.2: VP2](https://twitter.com/DetlefBurkhardt/status/1320322586153279488?s=20)
* [Day 87.3: BPMN](https://twitter.com/DetlefBurkhardt/status/1320391018592821248?s=20)
* [Day 88.1: plantuml](https://twitter.com/DetlefBurkhardt/status/1320773905146089472?s=20)
* [Day 88.2: more plantuml](https://twitter.com/DetlefBurkhardt/status/1320826231227637760?s=20)
* [Day 88.3: text-to-uml tools](https://twitter.com/DetlefBurkhardt/status/1320866283395731459?s=20)
* [Day 89: ahh groovy, much better!](https://twitter.com/DetlefBurkhardt/status/1321198300608036865?s=20)
* [Day 90: learning unreal animation](https://twitter.com/DetlefBurkhardt/status/1321529950139277313?s=20)
* [Day 91: unreal great animation kickstart](https://twitter.com/DetlefBurkhardt/status/1321961722756898817?s=20)
* [Day 92: two more unreal courses done](https://twitter.com/DetlefBurkhardt/status/1322643057343369217?s=20)
* [Day 93.1 image processing with R](https://twitter.com/DetlefBurkhardt/status/1322695439087509504?s=20)
* [Day 93.2: lab-of-labs update](https://twitter.com/DetlefBurkhardt/status/1322847355566215170?s=20)
* [Day 93.3: R for magick](https://twitter.com/DetlefBurkhardt/status/1322925475648446464?s=20)
* [Day 94: nothing](https://twitter.com/DetlefBurkhardt/status/1324155267781677057?s=20)
* [Day 95: moRe](https://twitter.com/DetlefBurkhardt/status/1324576510003580928?s=20)
* [Day 96: R-Magick](https://twitter.com/DetlefBurkhardt/status/1324885562826887174?s=20)
* [Day 97: Unreal Free Stuff](https://twitter.com/DetlefBurkhardt/status/1325208242981543936?s=20)
* [Day 98.1: R-Magick Workspace](https://twitter.com/DetlefBurkhardt/status/1325406067157819394?s=20)
* [Day 98.2: Inkscape as SVG-Editor](https://twitter.com/DetlefBurkhardt/status/1325434118570717194?s=20)
* [Day 98.3: Screen Ruler for fron andrijac](https://twitter.com/DetlefBurkhardt/status/1325507497789165571?s=20)
* [Day 98.4: Pop Art with R-Magick & Keith Haring](https://twitter.com/DetlefBurkhardt/status/1325595810424385536?s=20)
* [Day 99: Dancing Keith with rstats and rstudio](https://twitter.com/DetlefBurkhardt/status/1325948750934765568?s=20)
* [Day 100: One hundred again](https://twitter.com/DetlefBurkhardt/status/1326203695307100160?s=20)

### Daily Tweets for R4: (Break)
* [tuber](https://twitter.com/DetlefBurkhardt/status/1327695787128737794?s=20)
* [vscode 10/20](https://twitter.com/DetlefBurkhardt/status/1327977263980285953?s=20)
* [unxutils](https://twitter.com/DetlefBurkhardt/status/1329213172268535810?s=20)
* [r-moments](https://twitter.com/DetlefBurkhardt/status/1329547473258426370?s=20)
* [wsl2](https://twitter.com/DetlefBurkhardt/status/1329877026371723269?s=20)

### Daily Tweets for R4: (Stoppage Tine)
* [Day 101: Gradle](https://twitter.com/DetlefBurkhardt/status/1329954153238581249?s=20)
* [Day 102.1: Gradle Ideas](https://twitter.com/DetlefBurkhardt/status/1330093853517492224?s=20)
* [Day 102.2: Gradle Advantages](https://twitter.com/DetlefBurkhardt/status/1330158916035112970?s=20)
* [Day 102.3: Hello Gradle World](https://twitter.com/DetlefBurkhardt/status/1330208022589747203?s=20)
* [Day 102.4: Copy & Paste with Gradle](https://twitter.com/DetlefBurkhardt/status/1330274893536047115?s=20)
* [Day 102.5: What is the Gradle Wrapper](https://twitter.com/DetlefBurkhardt/status/1330310006084931587?s=20)
* [Day 103.1: Some Gradle Books Overview](https://twitter.com/DetlefBurkhardt/status/1330467914256932867?s=20)
* [Day 103.2: Gradle Book: Succinctly](https://twitter.com/DetlefBurkhardt/status/1330533550660186113?s=20)
* [Day 103.3: Gradle ant imperative](https://twitter.com/DetlefBurkhardt/status/1330579094728302596?s=20)
* [Day 103.4: Gradle Plugins Tags](https://twitter.com/DetlefBurkhardt/status/1330677235032928257?s=20)
* [Day 104: Wordclouds with R](https://twitter.com/DetlefBurkhardt/status/1331000044066119681?s=20)
* [Day 105: rtweet TwitteR](https://twitter.com/DetlefBurkhardt/status/1331769493698306049?s=20)






