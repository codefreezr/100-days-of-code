# 100DaysOf[No]Code Log ...
...Round 5 by CodeFreezr (#100DaysOverte)

<img width="200px" src="assets/r5.jpg"/>

The log of my #100DaysOf[No]Code, Round 5, challenge.

---
## Forword

My focus will be to program the Meta- & Multiverse. My #SocialVR Homeworlds are #Overte & #thirdroom. But inspecting other bullshit-free solutions (no NFT, no Crypto-Scam ...) is on the agends.

My Hashtags will #100DaysOfCode #R5|Day:1, #R5|Day:2 ... #R5|Day:99, #R5|Day:100. Because I work all day, some days I may not make it, in balance for example on weekends 2-4 hours devopsing, engineering, building & hacking.

For reasons I will toot my tweets first. That means I would create a mastodon first, then replicate this to my english twitter account.

### Possible Curiculum
- 60% Overte -> Overte.org
- 30% Thirdroom -> Thirdroom.io
- 10% Other Worlds
- Everything always related to Bits&Bäume, InfoSec, FOSSEAM & Cha0s

### Things where I want to improve my skills
Overte:
- Rough Understanding of all aspects of the Overte API.
- Portale einrichten, Small In-World Apps (like World-Clock)
- Broadcast Studio to Stream from
- Sync Button to emulate Public Viewing
- Pico4 Works
- Vereinsmitglied Nr. 23
- Marketing:
    - istio
    - Steam
- Avatars
    - How to create, rigg and use Avatars with own emotes
    - Solutions:
        - [in3D](https://avaturn.gitbook.io/product-docs/)
        - [UnionAvatar](https://unionavatars.com/)
        - [Mixamo](https://www.mixamo.com/#/)
        - [ReadyPlayerMe](https://readyplayer.me/)
        - ...
- Slash-Commands
- Server 
    - Setup with Docker
    - Setup with Debian like 7 does
    - Use local Server
    - Scale Server OnDemand

thirdroom:
- Build 2-3 Rooms
- Check WASM
- Understand Scene

Overall:
- How to combine both:
    - Same Avatars
    - Portals
    - Moving Mounts / Rooms
    - Same Chat

## Days

- R5|Day:1 Start-Message
- R5|Day:2 Call to join in 
- R5|Day:3 Hedgedoc + Gitlab = <3
- R5|Day:4 Me, myself and I: Starting with an [Avatar](https://lab.nrw/hedgedoc/UB3eOBTvRaqRZTTyvZresQ#)
- R5|Day:5 Letzer Blick in den Spiegel mit meinen Digital-Twin-Avataren.
- R5|Day:6 URL-Shortener yours installiert & konfiguriert.
- [R5|Day:7](https://chaos.social/@CodeFreezR/109702195031292339) Ausflug gitlab vs. github
- [R5|Day:8](https://y.lab.nrw/r5d8) Architecture Part 1 Directory, Domain, Places, Zones (Kugel, Würfel, )
- [R5|Day:9](https://y.lab.nrw/r5d9) Architecture Part 2 Overte Domain-Server
- [R5|Day:10](https://y.lab.nrw/r5d10) Architecture Part 3 Overte Domain-Server
- [R5|Day:10.2](https://y.lab.nrw/r5d10-2) List of five interessting Avatar-Apps
- [R5|Day:11](https://y.lab.nrw/r5d11) First Entity-Script: Opening an Overlay Web-Browser with a Click on an Entity 
- [R5|Day:12](https://y.lab.nrw/r5d12) Finishing of Overte-Avatars
- [R5|Day:13](https://y.lab.nrw/r5d13) Space Soccer in TrackTower|15 
- [R5|Day:14](https://y.lab.nrw/r5d14) Lift control in TrackTower|15
- [R5|Day:15](https://y.lab.nrw/r5d15) Openbrush v2.0
- [R5|Day:16](https://y.lab.nrw/r5d16) FOSDEM First Pivot
- [R5|Day:17](https://y.lab.nrw/r5d17) FOSDEM Fringe Sessions
- [R5|Day:18](https://y.lab.nrw/r5d18) FOSDEM Building Season 23, Part A
- [R5|Day:19](https://y.lab.nrw/r5d19) FOSDEM Building Season 23, Part B
- [R5|Day:20](https://y.lab.nrw/r5d20) FOSDEM Flightplan Day One
- [R5|Day:21](https://y.lab.nrw/r5d21) FOSDEM Flightplan Day Two
- [R5|Day:22](https://y.lab.nrw/r5d22) Maquette: 3D Painting like OpenBrush
- [R5|Day:23](https://y.lab.nrw/r5d23) F-DROID in VR
- [R5|Day:24](https://y.lab.nrw/r5d24) PI-Launcher
- [R5|Day:25](https://y.lab.nrw/r5d25) Duck Duck Go in VR
- [R5|Day:25](https://y.lab.nrw/r5d26) Sunday VRunch & iDamF in Matrix
- [R5|Day:26](https://y.lab.nrw/r5d26) iDamF
- [R5|Day:27](https://y.lab.nrw/r5d27) Code Android inside Android
- [R5|Day:28](https://y.lab.nrw/r5d28) I'm the Listenheini
- [R5|Day:29](https://y.lab.nrw/r5d29) AEG with termux
- [R5|Day:30](https://y.lab.nrw/r5d30) WebXR - Tutorial Part 1
- [R5|Day:31](https://y.lab.nrw/r5d31) WebXR - The Spec-Pages
- [R5|Day:34](https://y.lab.nrw/r5d34) WebXR - Tutorial Part 2
- [R5|Day:35-1](https://y.lab.nrw/r5d35-1) WebXR - Tutorial Part 3
- [R5|Day:35-2](https://y.lab.nrw/r5d35-2) WebXR - Frameworks (de)
- [R5|Day:35-2e](https://y.lab.nrw/r5d35-2e) WebXR - Frameworks (en)
- [R5|Day:36](https://y.lab.nrw/r5d36) WebXR - A-Frame with Glitch
- [R5|Day:37](https://y.lab.nrw/r5d37) Glitch: Custom Tags plus Javascript
- [R5|Day:38-39](https://y.lab.nrw/r5d38-39) More A-Frame
- [R5|Day:40](https://y.lab.nrw/r5d40) BabiaXR found @FOSDEM23




Future Ideas



aframe: primitives & attributes
aframe: collection
babylon.js: ...
godot 4.0 ...
kotlin

Godot 4.0 !!! -> OpenXR, WebXR

Blender XR / FreeBird
Prepare SXSW 23
Auflösung in NoManSky in der Pico
r5 nextopix
Wazupp supermedium.com?
a-scene primitives & attributs cross-reference
3D Modeller with Support for a-frame
rss sync
Bogen und Rumfahren



damf-XARt
- Sketch, Brush & Friends - A Channel for immersive Design, Art, Music & Fun (damf) between Openbrush, Gravity Sketch, MS Maquette for Blender & Games, SociaVR & the Multiverses, Overte & Thirdroom. Building Things & Worlds, Creating Flow's, Pipelines and event think about generative 3D Arts with Scripting in Openbruch and add Music & Sound to it.

- SQL Steampipe ...
- SQL: SQL Collection
- Running & Reading & Running
- Overte: Die unterschiedlichen Ziele der Scripte.
- Overte: Erstes "Hallo VR-World"
- Overte: gitpages for References
- OpenBrush Scripting - The Examples
- XR Graph
  - https://sidequestvr.com/app/1892/xr-graph
  - https://github.com/marlon360/xr-graph
  - https://xr-graph.vercel.app/?function=[b%20*%20u%20*%20cos(a%20*%20v),%20b%20*%20u%20*%20sin(a%20*%20v),%200.2%20*%20v]&uMin=-2&uMax=2&vMin=-10&vMax=10&xMin=-2&xMax=2&yMin=-1.9999311713564978&yMax=1.9999311713564978&aMin=0&aMax=1.1&a=0.8800000000000001&bMin=0&bMax=1&b=0.8
- Paint the Business Icons with OpenBrush -> Offer for Use
- Beatsaver.com plus Spotify-Premoum Download in MoonRider?
