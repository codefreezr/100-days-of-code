# 100 Days Of Kode - Log

![Her Majesty, the Queen of Kode](/images/kode.png)
> On Wednesdays we kode. :floppy_disk:

### Day 000: November 11, 2018, Sunday

**Today's Progress**: I began working through K&R (otherwise known as "The C Programming Language") and completed the book's first few exercises.

**Thoughts**: I've been meaning to find time to familiarize myself with C and it's exciting to finally start learning it!

**Link(s) to work**:
1. [K&R Exercises 1-1 & 1-2](/kr/hello.c)
2. [K&R Exercise 1-3](/kr/exercise1-3.c)
3. [K&R Exercise 1-4](/kr/exercise1-4.c)
4. [K&R Exercise 1-5](/kr/exercise1-5.c)
5. [K&R Exercise 1-6](/kr/exercise1-6.c)

### Day 001: November 12, 2018, Monday

**Today's Progress**: Completed the next 5 exercises in K&R.

**Thoughts**: So far, so good. The material has been pretty easy to grasp and I'm eager to advance further.

**Link(s) to work**:
1. [K&R Exercise 1-7](/kr/exercise1-7.c)
2. [K&R Exercise 1-8](/kr/exercise1-8.c)
3. [K&R Exercise 1-9](/kr/exercise1-9.c)
4. [K&R Exercise 1-10](/kr/exercise1-10.c)
5. [K&R Exercise 1-12](/kr/exercise1-12.c)

### Day 002: November 13, 2018, Tuesday

**Today's Progress**: Managed 2 exercises in K&R. I implemented the easier (horizontal) histograms in my solutions for the time being.

**Thoughts**: It'll undoubtedly irritate me if I don't also undertake the more challenging (vertical) histograms, so I'll likely do that tomorrow.

**Link(s) to work**:
1. [K&R Exercise 1-13](/kr/exercise1-13.c)
2. [K&R Exercise 1-14](/kr/exercise1-14.c)

### Day 003: November 14, 2018, Wednesday

**Today's Progress**: Successfully implemented vertical histograms in the previous 2 exercises; now I can consider those exercises complete.

**Thoughts**: I'd been thinking the vertical histograms would be easier to implement if multi-dimensional arrays had already been introduced, but it turned out to be fairly easy even with just one-dimensional arrays.

**Link(s) to work**:
1. [K&R Exercise 1-13](/kr/exercise1-13.c)
2. [K&R Exercise 1-14](/kr/exercise1-14.c)

### Day 004: November 15, 2018, Thursday

**Today's Progress**: Did 2 exercises in K&R which focused on functions and character arrays.

**Thoughts**: I had no problems with Exercise 1-15, but Exercise 1-16 proved to be challenging due to the condition that only the main routine was to be revised and because I needed to better my understanding of C input streams!

**Link(s) to work**:
1. [K&R Exercise 1-15](/kr/exercise1-15.c)
2. [K&R Exercise 1-16](/kr/exercise1-16.c)

### Day 005: November 16, 2018, Friday

**Today's Progress**: Completed Exercise 1-17 in K&R which further dealt with character arrays.

**Thoughts**: This exercise was simple enough to kode once I figured out how to approach it without the aid of multi-dimensional arrays. The answer? Storing individual line arrays together in a massive array!

**Link(s) to work**:
1. [K&R Exercise 1-17](/kr/exercise1-17.c)

### Day 006: November 17, 2018, Saturday

**Today's Progress**: Completed Exercise 1-18 in K&R which involved character arrays yet again!

**Thoughts**: I'm not going to lie - this exercise was a struggle. I kept trying to truncate the input lines from inside the 'copy' function but my output was consistently erroneous. I finally abandoned that idea, did the truncating inside of the main routine instead, and managed to get it to work! 

**Link(s) to work**:
1. [K&R Exercise 1-18](/kr/exercise1-18.c)

### Day 007: November 18, 2018, Sunday

**Today's Progress**: Completed Exercise 1-19 in K&R. And you bet your sweet bippy it revolved around character arrays!

**Thoughts**: The last exercise concerning character arrays before moving onward! This one was less stressful than yesterday's exercise and only required a minimal amount of experimentation before I stumbled upon a solution.

**Link(s) to work**:
1. [K&R Exercise 1-19](/kr/exercise1-19.c)

### Day 008: November 19, 2018, Monday

**Today's Progress**: Completed Exercise 1-20 in K&R.

**Thoughts**: It turns out that I was premature in stating that Exercise 1-19 was going to be the last exercise concerning character arrays. Although the next section detailed external variables and scope, the end-of-the-chapter exercises that followed still deal primarily with manipulating character arrays. I may or may not attempt them all.

**Link(s) to work**:
1. [K&R Exercise 1-20](/kr/exercise1-20.c)

### Day 009: November 20, 2018, Tuesday

**Today's Progress**: Worked on an HTML/CSS/JS project.

**Thoughts**: Decided to take a breather from K&R and work on revamping an anonymous project cloned from another repo. Don't worry, I'll return to C in due time!

**Link(s) to work**:
N/A

### Day 010: November 21, 2018, Wednesday

**Today's Progress**: Continued work on HTML/CSS/JS project.

**Thoughts**: One upside of this project is that it forces me to familiarize myself with other people's code, which I hear is important for growing one's skills as a programmer.

**Link(s) to work**:
N/A

### Day 011: November 22, 2018, Thursday

**Today's Progress**: Continued work on HTML/CSS/JS project.

**Thoughts**: The project is functional so now I am working to repurpose it for my needs which is always more fun!

**Link(s) to work**:
N/A

### Day 012: November 23, 2018, Friday

**Today's Progress**: Continued work on HTML/CSS/JS project. Yes, really.

**Thoughts**: I almost have this project to my liking. Might need a few more finishing touches but it's more or less finished!

**Link(s) to work**:
N/A

### Day 013: November 24, 2018, Saturday

**Today's Progress**: Wrote a simple Python script that reads in a text file of strings (separated by newlines), sorts them alphabetically, removes duplicate strings, and outputs the results into a new text file.

**Thoughts**: I needed a script to perform these tasks for me, so writing it was both practical *and* a nifty little challenge to help me refresh myself on Python's syntax! 

**Link(s) to work**:
1. [textSort.py](/misc/textSort.py)

### Day 014: November 25, 2018, Sunday

**Today's Progress**: Put together a script in JavaScript which (hopefully) obfuscates my email address on my GitLab Page.

**Thoughts**: In an attempt to thwart potential email harvesting bots, I wrote some JavaScript which displays a mailto link for my email address only after decoding the necessary strings several times from Base64 (and from an encoded URI where applicable) and concatenating them into a single string. Seems sufficiently muddled to me but an advanced enough bot with JavaScript enabled could probably still see through it, I suppose.

**Link(s) to work**:
1. [main.js](https://gitlab.com/swarns/swarns.gitlab.io/blob/master/public/js/main.js)

### Day 015: November 26, 2018, Monday

**Today's Progress**: Worked on that HTML/CSS/JS project some more.

**Thoughts**: I decided to return to the project and expand upon it. This web development stuff can be surprisingly addicting!

**Link(s) to work**:
N/A

### Day 016: November 27, 2018, Tuesday

**Today's Progress**: Continued work on HTML/CSS/JS project.

**Thoughts**: Added to and fine-tuned aspects of the site which houses the project. Presentation counts, after all!

**Link(s) to work**:
N/A

### Day 017: November 28, 2018, Wednesday

**Today's Progress**: Continued work on HTML/CSS/JS project.

**Thoughts**: I'm considering trying out the Pure.css framework for the project site. It seems far more minimal than Bootstrap which is something I appreciate. So many modern web frameworks are horrendously bloated!

**Link(s) to work**:
N/A
