# #100DaysOfCode Log - Round 3 by CodeFreezr

<img src="assets/R3-Logo-01.png"/>

The log of my #100DaysOfCode, Round 3, challenge.

From Day to day this will grow more and more into a weblog.

* Day0: [🐤](https://twitter.com/DetlefBurkhardt/status/1226472803517575169?s=20)

* Day1: [Welcome to R3](https://codefreezr.gitlab.io/lifelonglearning/en/posts/r3d01-welcome-to-r3/) -|- [🐤](https://twitter.com/DetlefBurkhardt/status/1226922515823570945?s=20)

* Day2: [Zen of Python](https://codefreezr.gitlab.io/lifelonglearning/en/posts/r3d02-zen-of-python/) -|- [🐤](https://twitter.com/DetlefBurkhardt/status/1227320232554967040?s=20)

* Day3: [From Scoop To Pip](http://bit.ly/cf-r3d3) -|- [🐤](https://twitter.com/DetlefBurkhardt/status/1227683128766074881?s=20)
 
* Day4: [Py & Code](http://bit.ly/cf-r3d4) -|- [🐤](https://twitter.com/DetlefBurkhardt/status/1228036307127021568?s=20)

* Day5: twypy 1#3  [🐤](https://twitter.com/DetlefBurkhardt/status/1228405766861787136?s=20)

* Day6: twypy 2#3 [🐤](https://twitter.com/DetlefBurkhardt/status/1228666057516253185?s=20)

* Day7: [twypy 3#3](http://bit.ly/cf-r3d7) -|- [🐤](https://twitter.com/DetlefBurkhardt/status/1229124664393375746?s=20)

* Day8: [Jumpstart](http://bit.ly/cf-r3d8to10) [🐤](https://twitter.com/DetlefBurkhardt/status/1229472842896592898?s=20)

* Day9: [more Jumpstart](http://bit.ly/cf-r3d8to10)  [🐤](https://twitter.com/DetlefBurkhardt/status/1229860008227213312?s=20)

* Day10: [pythonic Jumpstart](http://bit.ly/cf-r3d8to10) [🐤](https://twitter.com/DetlefBurkhardt/status/1230277680257536000?s=20)

* Day11: No Code Today #hanau [🐤](https://twitter.com/DetlefBurkhardt/status/1230617791063109632?s=20)

* Day12: No Code Today #hanau [🐤](https://twitter.com/DetlefBurkhardt/status/1230954949162434561?s=20)

* Day13: [Googletrans](http://bit.ly/cf-r3d13) [🐤](https://twitter.com/DetlefBurkhardt/status/1231349569214087174?s=20)

* Day14: cleanupy [🐤](https://twitter.com/DetlefBurkhardt/status/1231674635135180806?s=20)

* Day15: [Python Tutorial](http://bit.ly/cf-py-ger) [🐤](https://twitter.com/DetlefBurkhardt/status/1232047858494427149?s=20)

* Day16: starting mybag [🐤](https://twitter.com/DetlefBurkhardt/status/1232436008320323584?s=20)

* Day17: [wclock](http://bit.ly/cf-rd17) [🐤](https://twitter.com/DetlefBurkhardt/status/1232790194111361025?s=20)

* Day18: pydroid3 [🐤](https://twitter.com/DetlefBurkhardt/status/1233195924547145728?s=20)

* Day19: covid-19 [🐤](https://twitter.com/DetlefBurkhardt/status/1233462731015172096?s=20)
 
* Day20: collections [🐤](https://twitter.com/DetlefBurkhardt/status/1233926898847186947?s=20)

* Day21: [Raspy](http://bit.ly/cf-r3d21) [🐤](https://twitter.com/DetlefBurkhardt/status/1234286864342933505?s=20)

* Day22: more Raspy [🐤](https://twitter.com/DetlefBurkhardt/status/1234639028860997632?s=20)

* Day23: udemy 4 free [🐤](https://twitter.com/DetlefBurkhardt/status/1234959122317168640?s=20)

* Day24: codetutor [🐤](https://twitter.com/DetlefBurkhardt/status/1235332868081143810?s=20)

* Day25: atbs with python [🐤](https://twitter.com/DetlefBurkhardt/status/1235639214013849601?s=20)

* Day26: ImageMagick [🐤](https://twitter.com/DetlefBurkhardt/status/1236054728720691200?s=20)

* Day27: lowcode [🐤](https://twitter.com/DetlefBurkhardt/status/1236365136023166978?s=20)

* Day28: pandas on imdb [🐤](https://twitter.com/DetlefBurkhardt/status/1236741256178434048?s=20)

* Day29: pandas on ec2 [🐤](https://twitter.com/DetlefBurkhardt/status/1237150607733112842?s=20)

* Day30: Pandas recovered COVID-19 [🐤](https://twitter.com/DetlefBurkhardt/status/1237444696575705088?s=20)

* Day31: PandasReaderWriter [🐤](https://twitter.com/DetlefBurkhardt/status/1237545031491137537?s=20)

* Day32: [MobaXterm](http://bit.ly/cf-moba) [🐤](https://twitter.com/DetlefBurkhardt/status/1237916465870901251?s=20)
 
* Day33: Pandas on Jupyter [🐤](https://twitter.com/DetlefBurkhardt/status/1238254343868035078?s=20)

* Day34: PiClusterCloud [🐤](https://twitter.com/DetlefBurkhardt/status/1238544311039471616?s=20)

* Day35: ConversionSpecifiers [🐤](https://twitter.com/DetlefBurkhardt/status/1238933448787705858?s=20)

* Day36: More on Jupyter [🐤](https://twitter.com/DetlefBurkhardt/status/1239316436293693446?s=20)
 
* Day37: Regex [🐤](https://twitter.com/DetlefBurkhardt/status/1239685385892048897?s=20)
 
* Day38: Fight COVID-19 [🐤](https://twitter.com/DetlefBurkhardt/status/1240043262985060353?s=20)

* Day39: time-4-cli [🐤](https://twitter.com/DetlefBurkhardt/status/1240377431694983170?s=20)

* Day40: spotipy [🐤](https://twitter.com/DetlefBurkhardt/status/1240780227405635584?s=20)

* Day41 & 42: back-2-school [🐤](https://twitter.com/DetlefBurkhardt/status/1241503497989873664?s=20)

* Day43 files & folders  [🐤](https://twitter.com/DetlefBurkhardt/status/1242538658210754565?s=20)

* Day44-47 too much work  [🐤]( https://twitter.com/DetlefBurkhardt/status/1243684319538941954?s=20) 

* Day48 webframework  [🐤](https://twitter.com/DetlefBurkhardt/status/1244189721766608897?s=20)

* Day49-51 DjangoDockerStruggle  [🐤](https://twitter.com/DetlefBurkhardt/status/1245786954203086848?s=20)

* Day52 DockeUpdate [🐤](https://twitter.com/DetlefBurkhardt/status/1246209001983029250?s=20)

* Day53 Pythonic-4-All [🐤](https://twitter.com/DetlefBurkhardt/status/1246553676136751110?s=20)

* Day54 to-flask-or-not-to-flask [🐤](https://twitter.com/DetlefBurkhardt/status/1246939301683826689?s=20)

* Day55 [Flask On Docker](https://codefreezr.gitlab.io/lifelonglearning/en/posts/r3d55-flaskondocker/) [🐤](https://twitter.com/DetlefBurkhardt/status/1247620967062528000?s=20)

* Day56 Back-On-Track [🐤](https://twitter.com/DetlefBurkhardt/status/1247994903134720000?s=20)

* Day57 BeautifullSoup-v4 [🐤](https://twitter.com/DetlefBurkhardt/status/1248342434779738112?s=20)

* Day58 DragAndDrop-v1 [🐤](https://twitter.com/DetlefBurkhardt/status/1248731085149741056?s=20)

* Day59 DragAndDrop-v2 [🐤](https://twitter.com/DetlefBurkhardt/status/1249104766547955714?s=20)

* Day60 [Code Playgrounds](https://codefreezr.gitlab.io/lifelonglearning/en/posts/r3d60-playgrounds/) [🐤](https://twitter.com/DetlefBurkhardt/status/1249451993602707459?s=20)

* Day61 DragAndDrop-v3 [🐤](https://twitter.com/DetlefBurkhardt/status/1249885431623270400?s=20)

* Day62 atbStuff-with-Python: done. [🐤](https://twitter.com/DetlefBurkhardt/status/1250518692544339968?s=20)
 
* Day63 prep vue [🐤](https://twitter.com/DetlefBurkhardt/status/1250914739359821831?s=20)

* Day64 vuepress [🐤](https://twitter.com/DetlefBurkhardt/status/1251277198021087234?s=20)

* Day65 codepen.io [🐤](https://twitter.com/DetlefBurkhardt/status/1251632852275531777?s=20)

* Day66 vuebasics [🐤](https://twitter.com/DetlefBurkhardt/status/1252018441105571841?s=20)

* Day67 scrimba [🐤](https://twitter.com/DetlefBurkhardt/status/1252366953542377475?s=20)

* Day68 vegibit [🐤](https://twitter.com/DetlefBurkhardt/status/1252701707546644482?s=20) 

* Day69 vue-in-a-picture [🐤](https://twitter.com/DetlefBurkhardt/status/1253059801489293317?s=20)

* Day70-71 payware sucks [🐤](https://twitter.com/DetlefBurkhardt/status/1254142090604425221?s=20)

* Day72-73 acaguide [🐤](https://twitter.com/DetlefBurkhardt/status/1254872111031738371?s=20)

* Day 74 myVueBlue  [🐤](https://twitter.com/DetlefBurkhardt/status/1255247582667984897?s=20)

* Day 75 myComponentOne [🐤](https://twitter.com/DetlefBurkhardt/status/1255615148120772610?s=20)

* Day 76 <3 VueJS [🐤](https://twitter.com/DetlefBurkhardt/status/1256151471642546177?s=20)

* Day 77 webdev-snippets [🐤](https://twitter.com/DetlefBurkhardt/status/1256703017627389952?s=20)

* Day 78 json-vs-yaml [🐤](https://twitter.com/DetlefBurkhardt/status/1257082057970331654?s=20)

* Day 79 [from-erd/xsd-to-json/yaml](https://bit.ly/cf-r3d79) [🐤](https://twitter.com/DetlefBurkhardt/status/1257445726856122368?s=20)

* Day 80-82 no code these days [🐤](https://twitter.com/DetlefBurkhardt/status/1258512168862928896?s=20)

* Day 83 zen-2-emmet [🐤](https://twitter.com/DetlefBurkhardt/status/1258905674509103104?s=20)

* Day 84 custom-emmet [🐤](https://twitter.com/DetlefBurkhardt/status/1259273515687129089?s=20)

* Day 85 from-emmet-2-ahk [🐤](https://twitter.com/DetlefBurkhardt/status/1259576388354551808?s=20)

* Day 86 scoop+unxutils+ahk [🐤](https://twitter.com/DetlefBurkhardt/status/1260368557843656704?s=20)

* Day 87 VueJS-in-action [🐤](https://twitter.com/DetlefBurkhardt/status/1260694005546917888?s=20)

* Day 88 VueJS-in-action.2 [🐤](https://twitter.com/DetlefBurkhardt/status/1261092497968807943?s=20)

* Day 89 hugoagain [🐤](https://twitter.com/DetlefBurkhardt/status/1262087504917905409?s=20)

* Day 90 vuebooks [🐤](https://twitter.com/DetlefBurkhardt/status/1262466519117824006?s=20)

* Day 91 kindleswipe4me [🐤](https://twitter.com/DetlefBurkhardt/status/1262882739600138244?s=20)
 
* Day 92/1 widescreen-vatertag [🐤](https://twitter.com/DetlefBurkhardt/status/1263632676390567937?s=20)

* Day 92/2 masterfail [🐤](https://twitter.com/DetlefBurkhardt/status/1263828679064420353?s=20)

* Day 93 codesandbox [🐤](https://twitter.com/DetlefBurkhardt/status/1264138615317282816?s=20)

* Day 94 widescreen-triptych [🐤](https://twitter.com/DetlefBurkhardt/status/1264624216244133890?s=20)

* Day 95 compiled AHK [🐤](https://twitter.com/DetlefBurkhardt/status/1265077113738100736?s=20)

* Day 96 gitpod again [🐤](https://twitter.com/DetlefBurkhardt/status/1265390297695113216?s=20)

* Day 97 wsl2 [🐤](https://twitter.com/DetlefBurkhardt/status/1265790632896987137?s=20)

* Day 98 docker-on-wsl2 [🐤](https://twitter.com/DetlefBurkhardt/status/1266131734019690497?s=20)

* Day 99 helm4u [🐤](https://twitter.com/DetlefBurkhardt/status/1266510040736968704?s=20)

* Day 100 R3 Moment [🐤](https://twitter.com/DetlefBurkhardt/status/1266890577469214720?s=20)

* Day 101 helm v3 [🐤](https://twitter.com/DetlefBurkhardt/status/1267468109264441344?s=20)

* Day 102 jsonschema [🐤](https://twitter.com/DetlefBurkhardt/status/1267942878598266881?s=20)

* Day 103 motd [🐤](https://twitter.com/DetlefBurkhardt/status/1268652843227103234?s=20) 

* Day 104 wordcloud [🐤](https://twitter.com/DetlefBurkhardt/status/1269019452307374081?s=20)



---


