#! python3
# textSort.py - Converts lines in text.txt to lowercase,
# sorts them alphabetically while removing duplicates,
# and then writes the results to new_text.txt

textFile = open('text.txt', 'r')
textLines = textFile.readlines()
textWords = [word for word in textLines]
textWords = [element.lower() for element in textWords]
sortedWords = sorted(set(textWords))
with open('new_text.txt', 'w') as textNew:
    for words in sortedWords:
        textNew.write(words)
textFile.close()
textNew.close()
