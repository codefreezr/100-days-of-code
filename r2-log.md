# #100DaysOfCode Log - Round 2 by CodeFreezr


<img width="300" src="assets/100DaysR2.jpeg"/>

The log of my #100DaysOfCode, Round 2, challenge.


Table of Contents
=================

## Preparation-Episodes
#### top1

- [x] [R2:D0](#r2d0) <- Zero Day Introduction to R2
- [x] [R2:D1](#r2d1) <- Relocation hub 2 lab
- [x] [R2:D2](#r2d2) <- Curriculum is the new agenda
- [x] [R2:D3](#r2d3) <- to R or not to R
- [x] [R2:D4](#r2d4) <- VueJS not only on #Vuejsday
- [x] [R2:D5](#r2d5) <- EnterpriseEngineering from Zachman to SAFe 4.6
- [x] [R2:D19](#r2d19) <- CleanEnergyAndMobility
- [ ] R2:Dx <- ChaosMapping my way into the Chaos
- [ ] R2:Dx <- Flashtalks

Awesome LearnRwithR

## Coding-Episodes
#### top2
| **1/5** | **2/5**  | **3/5**  | **4/5**  | **5/5**  |
|:-------------|:-------------|:-------------|:---------------|:---------------|
| [R2:D6](#r2d6) Absolute BeginnR       | [R2:D26](#r2d26) ecars »R« us|...|...|...|
| [R2:D7](#r2d7) Swirl of cream         | [R2:D27](#r2d27) ecars »R« us|...|...|...|
| [R2:D8](#r2d8) Logical, or?           | [R2:D28](#r2d28) more ecars|...|...|...|
| [R2:D9](#r2d9) R-Mark-Blog-Bookdown   | [R2:D29](#r2d29) Sidekick LearnR & swirl|...|...|...|
| [R2:D10](#r2d10) DataCamp not only R  | [R2:D30](#r2d30) RPubs free 4 all|...|...|...|
| [R2:D11](#r2d11) DataCamp (2)         | [R2:D31](#r2d31) CRAN Topics|...|...|...|
| [R2:D12](#r2d12) DataCamp (3)         | [R2:D32](#r2d32) Awesome LearnRwithR|...|...|...|
| [R2:D13](#r2d13) swiRl again          | [R2:D33](#r2d33) make R cloudier!|...|...|...|
| [R2:D14](#r2d14) dplyr & tidyr swirls in my mind| [R2:D34](#r2d3435) aws.training|...|...|...|
| [R2:D15](#r2d15) Collector's Day| [R2:D35](#r2d3435) aws.training|...|...|...|
| [R2:D16](#r2d16) kaggle meets tidyverse| [R2:D36](#r2d36) eclipse che|...|...|...|
| [R2:D17](#r2d17) messy vs. tidy vs. ronaldo| [R2:D37](#r2d37) gitpod|...|...|...|
| [R2:D18](#r2d18) rstudio.cloud & googlesheets = -R-ocketSience|...|...|...|...|
| [R2:D20](#r2d20) Relational Datasets|...|...|...|...|
| [R2:D21](#r2d21) Shiny sneak-preview|...|...|...|...|
| [R2:D22](#r2d22) Server is a function|...|...|...|...|
| [R2:D23](#r2d23) Shiny by Examples|...|...|...|...|
| [R2:D24](#r2d24) More Shiny|...|...|...|...|
| [R2:D25](#r2d25) From swirl to learnR|...|...|...|...|



## Log

### Preparation-Episodes

---
#### R2D0
#### Zero Day Introduction to R2  
Round 2 for my #100DaysOfCode curriculum will start soon. In this round I will resurrect also my blogger genome and use this focused approach to re-established a daylie **"blog-post"**. But not with wordpress, blogger.com, just writing markdown based git repo files. And for this I want to move more from github to gitlab, not only becase gitlab is truels #FOSS but also because of the tons of new features like Web-IDE, Docker/K8-Integrations, CI/CD...  
Another slightly change I will dare, are the strict one-per-day rule. This caused some trouble in R1, esp. in high-voltage workload times. So I would handle this a bit more **elastic**. If I have time for 2-3 hours one day, I would create also 2-3 posts, perhaps buffered, if it is not possible to post stuff about 2-4 days it's also ok. Maintarget is to have at minimum 100 posts / commits to reach my curriculum goals.  
And for exactly this I will prepare stuff a bit more and add some **"Preperation Episodes"** before or perhaps in between to sort out, puzzle, design, layout concrete codings & hacks. So the next Days will introduced the curriculum bit deeper to give a reasoning for that.  
[tweet-r2d0](https://twitter.com/DetlefBurkhardt/status/1081343832866869252)  
[back-2-toc](#top1)  


---
#### R2D1 
#### Relocation hub 2 lab
![gitlab100.png](assets/gitlab100.png)   
One killing argument for gitlab: try "docker search gitlab", then try "docker search github". You can easily install the CE and the EE Edition of gitlab just with the snap of docker. Github is closed software, even my beloved #octocat is not free and but hold inside a prison of copyrights. So why should all the #FOSS projects choose a non-FOSS SCM as it's core? It's not Microsoft who bought Github last year. They have done a lot of good decisions, since Satya Nadella take CEO over in 2014.  
   
Gitlab is indeed the challenger, see https://stackshare.io/stackups/github-vs-gitlab, but they do a great job. They adding one killing feature after another, for e.g. Serverless, Kubernetes, Docker, CI/CD, WebIDE. It feels like a silence transformation from a git-based SCM Solution to a complete Cloud-Solution Provider. So it's more than consequent to move to gitlab. Here we go.  
[tweet-r2dx](https://twitter.com/DetlefBurkhardt/status/1081549700950568960)  
[back-2-toc](#top1)  


---
#### R2D2 
#### Curriculum is the new agenda
![r2d2-curriculum.png](assets/r2d2-curriculum.png)   
Why creating an agenda, why not just start hacking? Because it's me. I'm not only a coder, I'm also a designer, engineer, even an architect. So creating a plan, 
and try to follow them is part of my nature. In the next six episodes, I will introduce my focus, my toc-curriculum. Ah and as gopher was my personal masqot in season 1, 
r2d2 could this become in season 2.  
[tweet-r2d2](https://twitter.com/DetlefBurkhardt/status/1081887982288846848)  
[back-2-toc](#top1)  


---
#### R2D3
#### to R or not to R  
![r2d3-rstats-logo.png](assets/r2d3-rstats-logo.png)   
Besides my #Javascript #NodeJS vains, I have a wonderfull JVM-based "Mother-Language", called #ApacheGroovy under my skin. And for performance reason and to have platform-agnostic compiled executables I focused #golang in my last #100DaysOfCode. So why another Language?  
At first I want to dig into a real, plain functional programming language, to acclimate this approach, where possible, also in my "mother"-programming languages. 
I perceived, that I was too lazy to head into the functional road, because it worked the "old" way. So I hope to condition my self to think more "functional", not only Object-Oriented.
On the other side I'm very impressed of the R-features to manipulate data structures on any level, not only for #DataScience or #MachineLearning. 
You know, I'm docker addicted. To name the docker R-Base project "Rocker" makes me also more then curious. 
But after a first glance on RStudio and the swirl first-access, I was completely convinced, that R should be the next milestone in my polyglot journey through metamorphic rocks.  
[tweet-r2d3](https://twitter.com/DetlefBurkhardt/status/1082181304588947456)  
[back-2-toc](#top1)  


---
#### R2D4  
#### VueJS not only on Vuejsday
![r2d4-vuejs-logo.png](assets/r2d4-vuejs-logo.png)   
From plain vanila #js to #ajax and #jquery to #vuejs with sidekicks to #nodejs. Steps in the evolution of #javascript. VueJS is one of the most famous JS-Frameworks like #Angular or #react. VueJS instantly conquered my heart last year, because it's offers a really simplified entry point, did not make so many breaking changes like angular and seems to more independent than zuckbergs react. And with #vuepress and #nuxt there are two solutions how to combine the mighty of the that frameworks with the easyness of static side generators (SSG). In my last round of #100DaysOfCode I've streaked this and that, this round is for more toughtfullness. And I would remember the #vuejsday tag to post at least once per week stuff around vuejs.  
[tweet-r2d4](https://twitter.com/DetlefBurkhardt/status/1082478843305304064)  
[back-2-toc](#top1)  


---
#### R2D5 
#### EnterpriseEngineering from Zachman to SAFe 4.6
![r2d5-enterprises-logo.png](assets/r2d5-enterprises-logo.png)   
As Enterprise Engineer you have the complete picture, the complete company and even the #commongood in mind.  
DevOps, XaaS-Cloudcomputing, UML, CI/CD, EAM, ALM, SoftwareConfig- & Releasemanagement at Scale are only some puzzle-pieces to transform a company from waterfall-thinking for e.g. to [SaFe 4.x](https://www.scaledagileframework.com/).  
But we need not only transform IT-Projects & Companies, we need a transformation of the whole society (#gwoe #commongood #bge #ubi). Crazy? Think about this:
![transformation.jpg](assets/transformation.jpg)   
[tweet-r2d5](https://twitter.com/DetlefBurkhardt/status/1082955472162963456)  
[back-2-toc](#top1)  


---
#### R2D6 
#### Absolute BeginnR
![r2d6-absoluteBeginnR.png](assets/r2d6-absoluteBeginnR.png)   
Because in my R1 of 100DaysOfCode I was very impressed of [A Tour of Go](https://tour.golang.org). So I searched something comparable for R. And indeed their is a 
R Package called "swirl", which sounds like "A Tour of R".  
But should I really install and config all the stuff like R-Base and RStudio upfront? Yes, but you can do it as a Rocker (=> R on Docker) in just 6 simple steps.  
[tweet-r2d6](https://twitter.com/DetlefBurkhardt/status/1083288313446961152)  
[back-2-toc](#top1) 
  
---
#### R2D7  
#### Swirl of cream  
![r2d7-swirl-01.png](assets/r2d7-swirl-01.png)   
Swirl is a learning environment for R written in R. There are a lot [swirl learning courses](https://github.com/swirldev/swirl_courses) for DataScience. 
Today I start the first Lessons on the 15 Lessons Course "R Programming - The basics of programming in R".  
[tweet-r2d7](https://twitter.com/DetlefBurkhardt/status/1083849002804805633)  
[back-2-toc](#top1)  
  
  
---
#### R2D8 
#### Logical, or?
![r2d8-logical-lesson.png](assets/r2d8-logical-lesson.png)   
Can you guess it? Independet of the Programming Language combining logical operators can by tricky. So lesson Nr. x in the course "R Programming" is a great refresheR not only for #rstats.  
[tweet-r2d8](https://twitter.com/DetlefBurkhardt/status/1084048499023454208)  
[back-2-toc](#top1)  


---
#### R2D9 
#### R-Mark-Blog-Bookdown
![r2d9-rdown.png](assets/r2d9-rdown.png)   
After swirling a bit through the fancy basics of the R Language, I will proof the "Blogability" of the R-Language. How easy or uneasy is the support for creating blogs, writing 
articles or even books. In the #golang world I've was very happy with hugo SSG approach. For the R Ecosystem I will learn now R-Markdown, Bookdown and 
perhaps Blogdown and how to setup all the stuff.  
I found great OOTB-Example to fork here: [gitlab <3 bookdown](https://gitlab.com/RLesur/bookdown-gitlab-pages) 
Another guide to R Markdown [learning R Markdown in an hour](http://eriqande.github.io/rep-res-web/lectures/learning-rmarkdown-in-an-hour.html)
The official [Homepage bookdown.org](https://bookdown.org/) offers a one-click publishing out of RStudio. Sounds interessting.  
[tweet-r2d9](https://twitter.com/DetlefBurkhardt/status/1084378104578146305)  
[back-2-toc](#top1)  


---
#### R2D10 
#### DataCamp not only R
![r2d10-datacamp.png](assets/r2d10-datacamp.png)   
Now I understand why the swirl courses are not newer then 2015. Around 2014 Datacamp was founded. It's a phantastic collection of courses and guides for 
DataScience and R. With interactive online editor based coursed can you learn R, DataScien, SQL, Python and more without installing anything on your machine.
So I think I would spend some days here...  
[tweet-r2d10](https://twitter.com/DetlefBurkhardt/status/1084715790039961600)  
[back-2-toc](#top1)  



---
#### R2D11
#### DataCamp (2)
![r2d11-intro.png](assets/r2d11-intro.png)   
Originally my plan was to have a lazy january, repair my teeth, don't do project acquisition. family, learning, coding, sleeping. Buut then my telephone did not stand still,
so many offerings, and on top of that a flu ...  
Against all odd I have started "Introduction to R" at datacamp.com Progress 4 1/2 from 6.  
[tweet-r2d11](https://twitter.com/DetlefBurkhardt/status/1085401520437448704)  
[back-2-toc](#top1)  


---
#### R2D12 
#### DataCamp (3)
![r2d12-intro-r-ready.png](assets/r2d12-intro-r-ready.png)   
Completed my first datacamp course "Introduction to R Programming", nurse my cold.  
[tweet-r2d12](https://twitter.com/DetlefBurkhardt/status/1085864024699518976)  
[back-2-toc](#top1)  


---
#### R2D13
#### swiRl again
![r2d13-swirl-done](assets/r2d13-swirl-done.png)   
Back to swirl I've finishing the 15 chapters about "R Programming". The handy chapter 12 "Looking at data" offers some nice pre-checks you should consider before 
startung big data analysis. Every swirl learnings I have done, was donw inside rstudio.cloud. One of the best online editor. 
Comparable with cloud9, EclipseChe, codeanywhere or SourceLair.  
[tweet-r2d13](https://twitter.com/DetlefBurkhardt/status/1086140256888467457)  
[back-2-toc](#top1)  




---
#### R2D14 
#### dplyr & tidyr swirls in my mind
![r2d13-swirl-tidyverse.png](assets/r2d13-swirl-tidyverse.png)   
To get's my hands dirty in the tidyverse continuum, I've installed the swirl course [Getting and Cleaning Data](https://github.com/swirldev/swirl_courses/tree/master/Getting_and_Cleaning_Data) 
from the the swirl course repository. It's a pitty that this repo is just slim and the great pieces of R-Learning are all older then 2-4 years.  
What's about the hype around the tidyvers.org? They state this: "The tidyverse is an opinionated collection of R packages designed for data science. All packages share an underlying design philosophy, grammar, and data structures."   
And indeed after the checking the syntax of the first five basics functions of the dplyr-package select(), filter(), arrange(), mutate(), summarize() 
you understand the approach: Make things more simple. German-Zitat: Weil einfach einfach einfacher ist. Grandious!  
[tweet-r2d14](https://twitter.com/DetlefBurkhardt/status/1087188932813615107)  
[back-2-toc](#top1)  






---
#### R2D15 
#### Collector's Day
![r2d15-opendata.png](assets/r2d15-opendata.png)   
Today is more about collecting than coding. Learning a language is also learning all the books about the language. And in opposite of some other languages, there are
also some actual pile of R-Books in german. Probably I should stop sleeping, so many to learn, to read, to watch, to listen.  
But books are not my only chases today. While starting with R, there raises also a hunger for data, big and bigger data. Afte starting with a brand new account on 
kaggle today, I realized, that I don't have created an OpenData twitter-list. So here it comes: http://bit.ly/cf-opn-data  
[tweet-r2d15](https://twitter.com/DetlefBurkhardt/status/1087306781628477440)  
[back-2-toc](#top1)  


---
#### R2D16 
#### kaggle meets tidyverse
![r2d16-kaggle-meets-tidyverse.png](assets/r2d16-kaggle-meets-tidyverse.png)   
After learning the basice, collection some books, it's time jump into real-world-data. Ok #FIFA19 is not only real. Anyway I start my first own steps
in Data-Analysis with putting datasets from kaggle into the tidyverse.  
[tweet-r2d16](https://twitter.com/DetlefBurkhardt/status/1087974555824652288)  
[back-2-toc](#top1)  

tidy up all but neymar
fifa1 <- fifaRaw %>% separate(Name, c("Name1", "Name2"))



---
#### R2D17 
#### messy vs. tidy vs. ronaldo
![r2d17-messy-vs-tidy-vs-ronaldo](assets/r2d17-messy-vs-tidy-vs-ronaldo.png)   
Today I have solved a typical messy data problem: One single column for sur- and prename, some rows without prename, some with middle-names. Here comes the
powerfil tidyr.separate to the rescue:
```
fifaClean <- fifaRaw %>% separate(Name, c("Prename", "Surname"), extra="merge", fill="left")
```
At first I thought I would assign a league to a club. But this could be changed from season to season. I will put this as a bookmark until I'm familiar with timeseries. Until then I would 
try to add country and the national football associations. Ha! And here becomes the data offerings a bit dense. Perhaps my first kaggle dataset?  
[tweet-r2d17]()  
[back-2-toc](#top1)  


---
#### R2D18 
#### rstudio.cloud & googlesheets = R-ocketSience  
![r2d18-googlesheet.png](assets/r2d18-googlesheet.png)   
The more I learn about R, the more excited I become. At first rocker & swirl, then datacamp, then R- Blog-/Markdown, RStudio and RStudio.cloud, after this all 
about the tidyverse. And now library("googlesheets"); An awesome match made in heaven. And I feel a bit this is not the last r-gem I will digg.  
Now I can r-based manage, juggle, tidy, crud all my googlesheets; And velieve me, I have a lot of them.  
1000 thx to Jenny Bryan & Joanna Zhao.  
  
4 Steps to start the googlesheets-to-heaven-train-ride:

```
install.package("googlesheets")
library(googlesheets)
gs_auth()
gs_ls()
```

Tadaa, all your Google-Sheets offered inside a tibble. Maximum awesome!  

Ok you better load the most actual version with  

```
github_install("jennybc/googlesheets")
```

For this you have to install and load the devtools. If it is not allready installed, use:

```
install.packages("devtools")
library(devtools)
```

Loading a document and a workingsheet, here the 2nd one, into a tibble is then only a smart two-liner:

```
ff <- gs_title("fifa19-Sheet")
fifaClubs <- gs_read(ff,ws=2)
```
[tweet-r2d18](https://twitter.com/DetlefBurkhardt/status/1088356653395787777)  
[back-2-toc](#top1)  


---
#### R2D19 
#### CleanEnergyAndMobility
![r2d18-cleanenergyandmobility.png](assets/r2d18-cleanenergyandmobility.png)   
One important content topic for my R2 Learning Days is CleanEnergyAndMobility. In this we have the key to destroy or save earth. #FridayForFuture is one important hashtag.
If we can mainstream a mix of solar-, water-, wind- and hydrogen-power; ban co2/nox cars out of our cities as soon as possible, than we have perhaps a chance. For building
a social standing ground, I've started a list and split my dual-language moments into two: one for english, one for german.  
* CLEAM Moment (de) (https://twitter.com/i/moments/1088848715136278529)  
* CLEAM Moment (en) (https://twitter.com/i/moments/942117639131942919)  
* CLEAM Twiter List (https://twitter.com/DetlefBurkhardt/lists/cleanenergyandmobility)  

[tweet-r2d19](https://twitter.com/DetlefBurkhardt/status/1089505965232267264)  
[back-2-toc](#top1)  


---
#### R2D20 
#### Relational Datasets
![r2d20-merge.png](assets/r2d20-merge.png)   
Well known stuff with different verbs. Even if Venn is not super exact a Cartesian product it's a handy mnemnomic for the diffent cases to merge two datasets.  
* [Chapter 13](https://r4ds.had.co.nz/relational-data.html) of #R4DS was one of my learning basics  
  
[tweet-r2d20](https://twitter.com/DetlefBurkhardt/status/1090747901901529088)  
[back-2-toc](#top1)  


---
#### R2D21
#### Shiny sneak-preview
![r2d21-shiny-pit-stop.png](assets/r2d21-shiny-pit-stop.png)   
Because I found this free complete Datacamp Course I stop builing my first kaggle datasets and pit stop here. My first impression: Shiny seems cool, but needs some r upfront.
The course itself becomes more and more learning by debugging. I'm not sure if I want this, because it focused me more on how to make errors, then how to write clean code.
Anyway it was a great light snack before jumping back into BI/AI/ML stuff again.  
* [Free Shiny Datacamp Course](http://bit.ly/free-shiny-r-dtcmp)

[tweet-r2d1](https://twitter.com/DetlefBurkhardt/status/1090953493161132033)  
[back-2-toc](#top1)    
  

---
#### R2D22 
#### Server is a function.
![r2d22-server-is-a-function.png](assets/r2d22-server-is-a-function.png)   
Learning rstats shiny is learning also the basics about the most important reactive JS-Frameworks like #Angular, #Vue, #React and more. One of the main aspects, esp for the so called 
Single Page Application (SPA) is that former server logic wents to the edge, to the client. And what shows this simplieR and cleareR as in a shiny-app basic template.  
[tweet-r2dx]()  
[back-2-toc](#top1)  



---
#### R2D23 
#### Shiny by Examples
![r2d23-shiny-by-example.png](assets/r2d23-shiny-by-example.png)   
If you followed my R1 you know I'm a fan of learning by examples, watchout for #gobyes. Reading good code is the best basis to writing good code.
If you want learn the basics of shiny, just load/install shiny:

```
install.packages("shiny")
library(shiny)
```

After this you can run up to 11 examples, which opens in showcase mode. So you can read code, while inspecting the app. Great!

```
runExample("01_hello")      # a histogram
runExample("02_text")       # tables and data frames
runExample("03_reactivity") # a reactive expression
runExample("04_mpg")        # global variables
runExample("05_sliders")    # slider bars
runExample("06_tabsets")    # tabbed panels
runExample("07_widgets")    # help text and submit buttons
runExample("08_html")       # Shiny app built from HTML
runExample("09_upload")     # file upload wizard
runExample("10_download")   # file download wizard
runExample("11_timer")      # an automated timer
```
[tweet-r2d3]()  
[back-2-toc](#top1)  

  
---
#### R2D24 
#### More Shiny
![r2d24-shiny-by-example.png](assets/r2d24-shiny-by-example.png)   
From example to example I become more and more exited about the next shinyApp. Functional & reactive = R.  
[tweet-r2d4](https://twitter.com/DetlefBurkhardt/status/1092851665311617025)  
[back-2-toc](#top1)  


---
#### R2D25 
#### From swirl to learnR
![r2d25-learnr.png](assets/r2d25-learnr.png)   
Today I follow the path from swirl to learnR. You remember swirl? It is the "learning R with R" CLI Learning Environment I loved in the beginning. But there are for some
years no new swirl courses. Perhaps the created went to datacamp, or perhaps another successor raises: LearnR. This CRAN Module offers great 
capabilities for interactive guides, tutorials and learning courses at all. You can include Multiple-Choice, code editings, videos & much more.
It is based on RMarkdown and creates a shiny app.  

* [Homepage](https://rstudio.github.io/learnr/)
* [git-repo](https://github.com/rstudio/learnr)
* [CRAN-Entry](https://cran.rstudio.com/web/packages/learnr/index.html)
[tweet-r2d25](https://twitter.com/DetlefBurkhardt/status/1093431761315549184)  
[back-2-toc](#top1)  

  
---
#### R2D26 
#### ecars »R« us
![r2d26-ecars-01.png](assets/r2d26-ecars-01.png  )  
My first own miniproject is about ecars. My "dreamliner" would be a cityflitzer, sexy like the @microlino-isetta , cheap like a Renault Kwid with charging capabilities
via sun (no cost), hydrogen (faster recharge), EV charging station (spreaded infrastructur). On top it should incoporate hybrid-style self-recharging while braking 
or down the mountain. Until then we have analyse what the market will be offer. For this I will scan a lot of possible sources, like wikipedia, german KBA (Kraftfahrbundesamt) and more.
I would extract, clean and combine data, create a kaggle dataset, a shiny-app and perhaps a CRAN-Package. On top I will think about a pipeline how to keep stuff uptodate.  
[tweet-r2d26](https://twitter.com/DetlefBurkhardt/status/1094158621569474560)  
[back-2-toc](#top1)  


---
#### R2D27 
#### ecars »R« us
#### 
![r2d27-ecars-02.png](assets/r2d27-ecars-02.png)   
Harmonising international data-sets with km/h vs. mph or € vs. $; Different recharge requirements (120V, 220V, 240V, Public DC, etc. pp.) are some issues, 
which makes a complete automatic sync with different data-source challenging.  
[tweet-r2d27](https://twitter.com/DetlefBurkhardt/status/1094869063443140608)  
[back-2-toc](#top1)  



---
#### R2D28 
#### more ecars
#### 
![r2d28-ecars-03.png](assets/r2d28-ecars-03.png)   
Deep Dive into the world of #EV (Electronic Vehicle) resp. #ecars. From a simplofied electric-kettcar up to tesla model x I found
a lot of concepts and ideas. Really skewed are the high prices for an electric variant of an existing model, because an electric-motor
is much, much simpler. Because in this data acquisitions phase programming is not that much. Bit cleaning here, some splittings there;
mostly web -> googlesheet <-> rstudio.  
[tweet-r2d28](https://twitter.com/DetlefBurkhardt/status/1095250664844865536)  
[back-2-toc](#top1)  



---
#### R2D29 
#### Sidekick LearnR & swirl
![r2d29-awesome-learnr.png](assets/r2d29-awesome-learnr.png)   
Today I started a little side story. Collecting Stuff around LearnR and swirl. Because I believe in the approach learning R in R and so I like the swirl and learnR Packages,
but find not a registry of courses, tutorials, guides and more made with learnR or swirl.  
[tweet-r2d29](https://twitter.com/DetlefBurkhardt/status/1095983064025567232)  
[back-2-toc](#top1)  



---
#### R2D30 
#### RPubs free for all
![r2d30-rpubs.png](assets/r2d30-rpubs.png)   
With so many great discoveries, it's really hard to concentrate on one r-gem a bit longer. #Rpubs is the next great one. This is really a one-click-publishing from 
R-Markdown in RStudio to rpubs.com. Awesome!  
[tweet-r2d30](https://twitter.com/DetlefBurkhardt/status/1096796638591496192)  
[back-2-toc](#top1)  


---
#### R2D31 
#### CRAN Topics
![r2d31-cran-topics.png](assets/r2d31-cran-topics.png)   
My main problem with the 14.7K CRAN Packages was the missing "tags". To find cluster of packages, or see simple all areas you are lost without tags. Even libraries.io and rdrr.io offers
a more sophistacted search interface for R-Packages, but without missing tags there is a missing link.  
One step to mitigate this missing tags is the cran task view (ctv) idea. In 39 Topics you find a wide range of R-Packages grouped by a task/topic. 
The best part is the curated introductions to the topics. To read them you got a real complete overview of nearly all aspects of DataScience, ML, AI and more 
in R-Universe.  

* [CRAN Topics](https://cran.r-project.org/web/views/)
* [Libraries](https://libraries.io/cran)
* [rdrr.io](https://rdrr.io/)

[tweet-r2d31](https://twitter.com/DetlefBurkhardt/status/1097162232637677568)  
[back-2-toc](#top1)  


---
#### R2D32 
#### Awesome LearnRwithR
![r2d32-awesome-learnRwithR.png](assets/r2d32-awesome-learnRwithR.png)   
Today I finished a first version of an awesome list of learnR and {swirl} relatated resources. Yes it's github not lab, thats because the complete awesome series runs on it.
Find the github-page [here](http://bit.ly/learnRwR).  
[tweet-r2d32](https://twitter.com/DetlefBurkhardt/status/1097994436599078913)  
[back-2-toc](#top1) 


---
#### R2D33 
#### make R cloudier!
![r2d33-cloudyr.png](assets/r2d33-cloudyr.png)   
Today I found and explore another R-Gem Collection: cloudyr. R-Wrapper for important cloud API's inside "the cloudyr project". To use cloudyr R-Packages has the same
advantage then using tidyverse. All of them has the same basic structure, for e.g. how to store aws key-pairs. Beside the 17 aws R-Packages there are also packages for 
google and azure.  
[tweet-r2d33](https://twitter.com/DetlefBurkhardt/status/1099699606483542017)  
[back-2-toc](#top1)  


---
#### R2D3435 
#### aws.training
![r2d34-35-awstraining.png](assets/r2d34-35-awstraining.png)   
Yes #AWSSummit Berlin was a great place to meet & great. To find old and start new friends. And beside aws itself all my beloved tools I work a lot are also their. 
Like Jenkins, Elasticsearch, logz.io, spotinst & more. All the talks was also very instructive, so I've learned a lot and decide to bring this learnings also in 
my course. The section is Enterprise Engineering. And probably the most important startpage for learning aws-stuff is aws.training.
[tweet-r2d34](https://twitter.com/DetlefBurkhardt/status/1101791581571637248)  
[back-2-toc](#top1)  


---
#### R2D36 
#### eclipse che
![r2d36-eclipse-che.png](assets/r2d36-eclipse-che.png)   
After cloud9 and rstudio.cloud I examined eclipse che today and have to say: Not that bad. Apart from the somewhat slow Openshift connection, the move from the local Eclipse to a Cloud Eclipse has succeeded well.  
[tweet-r2dx](https://twitter.com/DetlefBurkhardt/status/1104800612464644097)  
[back-2-toc](#top1)  


---
#### R2D37 
#### gitpod
![r2d37-gitpod.png](assets/r2d37-gitpod.png)   
If you often switch between #golang, #js/#javascript, #python, #groovylang, #powershell, #bash and storing your stuff on #github or #gitlab this #WebIDE is for you. 
Awesome how easy is the editor to use:

```
https://gitpod.io#[git-url]
```

And voilá. Everything is done. You can start editing with a very well #vscode feeling online. A real 1-click git cloud-/web-editor. 
The editor is based on eclipse theia which also use the #vscode language server.  At the moment this editor supports only github, but gitlab support is almost ready.  
[tweet-r2d37]()  
[back-2-toc](#top1)  


---
#### R2D38 
#### WB into Spax EA v14
![](assets/)   
After the long break, now comes an enterprise engineering season in my R2-Volume with UML, BPMN, CMMN, DMN, Cloud Architecture Modelling, IaC, and lot more. For 
today I have installed #EA v14.1 and learned where all the Menu are gone, and how to find everything after leaving #EA at v10.
[tweet-r2dx]()  
[back-2-toc](#top1)  


---
#### R2D39 
#### Putting AWS Symbols into Sparx EA (#PasiSpea)
![](assets/)   
Hm, I', woundering that probably nobody took all AWS Symbols and Icons into a Sparx EA MDG to offer Toolboxes and more for modelling AWS Architecture within EA.
An because I did not found anything here, I start a sub-season in that direction. At first I want to analyse all the icons and the 2018 3D Icon-Style to the 
2019 2D Style. In my opinion the 3D Style rocks much more. Hopefully they support 3D also in the future. To start here I bring alle Icons at first as Images-Assets
into Sparx EA.
[tweet-r2dx]()  
[back-2-toc](#top1)  


---
#### R2D40 
#### PasiSpea - AWS Compute
Let's start today with learning all the services and sessources in the aws-compute section, while including the image assets into Sparx EA. It's a pitty that EA did
not support svg or eps, so I will start with just png bitmaps.
![](assets/)   
...
[tweet-r2dx]()  
[back-2-toc](#top1)  


---
#### R2D41
#### PasiSpea - AWS Storage 
![](assets/)   

...
[tweet-r2dx]()  
[back-2-toc](#top1)


---
#### R2D41 -  
#### PasiSpea - AWS Database 
![](assets/)   

...
[tweet-r2dx]()  
[back-2-toc](#top1)  


---
#### R2Dx 
#### 
![](assets/)   

...
[tweet-r2dx]()  
[back-2-toc](#top1)  


---
#### R2Dx 
#### 
![](assets/)   

...
[tweet-r2dx]()  
[back-2-toc](#top1)  